TranspaRanking
==============

The TranspaRanking system is a multi-dimensionally weighted, facetted evaluation survey / poll for media (i.e. music) people have posted and are fond of competing.

What are the differences to other systems
-----------------------------------------

1. It uses a small footprint of technology, avoiding modern approaches of mere development comfort means. It is done only with python3, Flask, jquery / JQueryUI and sqlite3, so it is lightweight.

2. Every voter can select from a menu, and add further facets if they cannot find suitable ones among those other voters are considering. The host can suggest facets and determine certain ones mandatory. Voters weight facets in which regard they want to evaluate the items. The weight value must be between 0 (mandatory 1) and the number of facets considered.

3. It is "GDPR-safe" as no person-related data is stored concerning the TranspaRanking instance. Only a decently secret, machine-generated part of the vote is stored on the server-side, namely only diced sequences of used facets and evaluated items. Votes, facet weights and annotations (verbal vote reasonings) are stored in a text-based vote record the voter needs to post externally e.g. in a web forum or social medium thread below the competition announcement. So, actually, the person-related share of the information is outsourced to someone's other GDPR responsibility realm but that there is not of much value without the secret sequences on the TranspaRanking server instance.

   The host of the competition finally needs to re-collect the vote records so the system can generate statistical reports and the ranking.

4. The vote records cannot be tampered with afterwards, that is, they would get invalid because the cryptographically secured HMAC signature would no longer match.

5. Invited votes: Voters can get one-time invitation tokens by the host once they is sincere the voter is unique and human.

   One can also vote without invitation but the vote will have reduced value. The value is in fact devided by the number of invited votes. The more votes are invited among all votes, the higher is their weight against the free (potentially bot-)votes. Invited votes that have not be re-collected are indicated as missing in the reports. Missing free votes are ignored.

   Invited votes can be changed until the end date-time of the competition. The voter must set a pass phrase to enable revision or revocation. The invitation token is then deleted from database, re-use impossible. 

   What measures the host undertakes to proove unity and identity of someone asking for invitation is left their business.
    
Structure of a published vote record
------------------------------------

```
# Host of competition at URL https://transparanking.server.tld/some-category/competition-label, please respect my vote record as follows:
W4xyz:h1X4ddyMnh3gADIx 2 1 2 3 2,2,0,3;1,{1[sounds a bit harsh in the treble region]},2,4;3,1,2,0;1,1,2,0;2,3,1,1;2,1,1,2;2,1,0,{1[nice triplets]};1,3,1,2
```

* `W4xyz`: random participation handle implicitly associated to a poll. Bulks of vote records are only verified and processed if they all are associated to the same poll.
* `h1X4ddyMnh3gADIx`: HMAC digest
* `2 1 2 3`: facet weights. To which facets the weight value relate is diced and saved on server side. The HMAC-signed material comprises the resolved order of actual facet identifiers (addedPos numbers, actually) to which these weights are appended, separated by ":" each.
* `2,2,0,3`: evaluation of a specific item in regard to the implied facets in secret order. The order of items, i.e. the order of identifiers (addedPos numbers, actually) is stored in the database, too. In the signed material, the addedPos are prepended to the vote numbers which concern them, separated by ":".
* `{1[sounds a bit harsh in the treble region]}`: annotation, will be included in the final report.
