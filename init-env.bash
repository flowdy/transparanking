#!/bin/bash
# Environment initialization in Bash-compatible shells
export PROJECT_BASE=$(cd $(dirname $BASH_SOURCE); pwd)
exec {out}<<'EOF'
printf "Starting %s in TRANSPARANKING development environment.\n" $SHELL
source /etc/bash.bashrc
source ~/.bashrc
source $PROJECT_BASE/venv/bin/activate
export PATH=$PATH:$PROJECT_BASE/scripts
alias cdp="cd $PROJECT_BASE"
PS1='TRANSPARANKING(\u@\h:\w) \$ '
HISTFILE=$PROJECT_BASE/.bash_history 
cdp
EOF

exec $SHELL --rcfile /dev/fd/"$out" # implies exit(), what follows ignored.
