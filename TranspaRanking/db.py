import datetime
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import UniqueConstraint
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column, relationship
from typing import List

class Base(DeclarativeBase):
  pass

db = SQLAlchemy(model_class=Base)

class PollQuestion(db.Model):
    pollId: Mapped[int] = mapped_column(db.Integer, primary_key=True)
    label: Mapped[str] = mapped_column(db.String, nullable=False)
    generalQuestion: Mapped[str] = mapped_column(db.String, nullable=False)
    endDate: Mapped[datetime.datetime] = mapped_column(db.DateTime, nullable=False)
    revocationCode: Mapped[str] = mapped_column(db.String, nullable=False)
    mode: Mapped[int] = mapped_column(db.Integer, nullable=False)
    announcementUrl: Mapped[str] = mapped_column(db.String, nullable=False)
    items: Mapped[List["Item"]] = relationship(back_populates="poll")
    facets: Mapped[List["Facet"]] = relationship(back_populates="poll")
    participants: Mapped[List["Participant"]] = relationship(back_populates="poll")
    invites: Mapped[List["Invitation"]] = relationship(back_populates="poll")

class Item(db.Model):
    itemId: Mapped[int] = mapped_column(db.Integer, primary_key=True)
    pollId: Mapped[int] = mapped_column(db.ForeignKey(PollQuestion.pollId))
    addedPos: Mapped[int] = mapped_column(db.Integer)
    label: Mapped[str] = mapped_column(db.String)
    url: Mapped[str] = mapped_column(db.String, unique=True) # may be a revocation count that does not start with 'http'
    poll: Mapped["PollQuestion"] = relationship(back_populates="items")
    __table_args__ = (UniqueConstraint('pollId', 'url', name='_uni_poll_items'),)

class Facet(db.Model):
    facetId: Mapped[int] = mapped_column(db.Integer, primary_key=True)
    pollId: Mapped[int] = mapped_column(db.ForeignKey(PollQuestion.pollId))
    addedPos: Mapped[int] = mapped_column(db.Integer)
    predefinedMandatory: Mapped[bool] = mapped_column(db.Boolean)
    question: Mapped[str] = mapped_column(db.String)
    poll: Mapped["PollQuestion"] = relationship(back_populates="facets")

class Participant(db.Model):
    participationHandle: Mapped[str] = mapped_column(db.String, primary_key=True)
    pollId: Mapped[int] = mapped_column(db.ForeignKey(PollQuestion.pollId))
    facetsSeq: Mapped[str] = mapped_column(db.String)
    itemsSeq: Mapped[str] = mapped_column(db.String)
    allowRevisionSecret: Mapped[str] = mapped_column(db.String)
    poll: Mapped["PollQuestion"] = relationship(back_populates="participants")

class Invitation(db.Model):
    inviteToken: Mapped[str] = mapped_column(db.String, primary_key=True)
    pollId: Mapped[int] = mapped_column(db.ForeignKey(PollQuestion.pollId))
    poll: Mapped["PollQuestion"] = relationship(back_populates="invites")

