import os, sys

from TranspaRanking.db import db as trdb, PollQuestion


def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY='dev',
        SQLALCHEMY_DATABASE_URI = 'sqlite:///transparanking.sqlite'
    )
    trdb.init_app(app)

    with app.app_context():
        trdb.create_all()

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    # a simple page that says hello
    @app.route('/')
    def hello():
        return render_template('hello.html')

    @app.route('/disclaimer')
    def disclaimer():
        return render_template('disclaimer.html', version='?')

    @app.route('/manage-poll/<path:label>')
    def create_poll(label):
        pq = PollQuestion(label=label)
        query = trdb.select(PollQuestion).filter_by(label=label)
        if (m := trdb.session.execute(query).scalar_one_or_none()):
            if m.revocationCode == request.form["revocationCode"]:
                return render_template('manage-poll.html', **(m.as_dict()))
            else:
                return "Poll already exists", 400
        if request.method == 'GET':
            return render_template("manage-poll.html", label=label)

    @app.route('/vote/<path:label>')
    def vote(label):
        return (
             "<h1>Not found</h1> Poll has not been configured or started. "
             "It might have been deleted or suspended (mode=0), then possibly exported and moved."
        )

    return app


if True: # 'uwsgi' in sys.modules: # TODO: re-activate later for component testing
    from flask import (
            Flask, render_template, request, jsonify, make_response, redirect,
            send_file, url_for
        )
    from werkzeug.security import generate_password_hash, check_password_hash
    app = create_app()
